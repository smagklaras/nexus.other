﻿using System;
using RestApiStubs.Models;

namespace RestApiStubs.Interfaces
{
    public interface ILogger
    {
        string GetLogForPaymentId(string id);

        string RootPath { get; set; }
        void AddObject<T>(T log) where T : class;
        OrderFilePaymentDetails GetPayment(string id);
    }
}