﻿using System.Web.Http;

namespace RestApiStubs
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/",
                defaults: new { controller = "Paylink", id = "wibble" }
            );
        }
    }
}
