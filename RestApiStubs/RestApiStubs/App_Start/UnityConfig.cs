using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using RestApiStubs.Interfaces;
using RestApiStubs.Models;

namespace RestApiStubs
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private readonly static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            
            container.RegisterType<ILogger, NDatabaseLogger>();

            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer Container()
        {
            return container.Value;
        }
        #endregion
    }
}
