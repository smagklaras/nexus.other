﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using RestApiStubs.Interfaces;
using RestApiStubs.Models;

namespace RestApiStubs.Controllers
{
    public class PaylinkController : ApiController
    {
        private readonly ILogger logger;

        public PaylinkController(ILogger logger)
        {
            this.logger = logger;
        }

        [Route("Paylink")]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "TRAXX Paylink Stub-o-mock");
        }

        // GET /TRAXX/Paylink/{id}
        [Route("Paylink/{id}")]
        public HttpResponseMessage Get(string id)
        {
            logger.RootPath = HostingEnvironment.MapPath("~/");

            OrderFilePaymentDetails paymentDetails = OrderFilePaymentDetails.Create(id, logger);

            return Request.CreateResponse(HttpStatusCode.OK, paymentDetails);
        }

        [Route("Paylink/{id}/{status}")]
        public HttpResponseMessage Post(string id, string status, [FromBody] PaymentPushRequest request)
        {
            logger.RootPath = HostingEnvironment.MapPath("~/");

            logger.AddObject(
                new PaylinkLog
                {
                    Id = id,
                    Message = string.Format("Payment received with a status of: {0}", status),
                    Timestamp = DateTime.Now
                }
            );

            // Only 'Paid' action allowed
            if (status.ToUpperInvariant() != "PAID" || request == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            OrderFilePaymentDetails payment = logger.GetPayment(id);

            //not there yet
            if (payment == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            //Already paid for
            if (payment.PaidStatus == PaymentStatus.Paid)
            {
                return Request.CreateResponse(HttpStatusCode.Gone);
            }

            UpdatePaymentDetails(payment, request);
            logger.AddObject(payment);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private static void UpdatePaymentDetails(OrderFilePaymentDetails paymentDetails, PaymentPushRequest pushRequest)
        {
            if (paymentDetails == null || pushRequest == null)
            {
                return;
            }

            paymentDetails.InvoiceNumber     = pushRequest.PaymentProviderReference;
            paymentDetails.PaymentMethodCode = pushRequest.PaymentMethodCode;
            paymentDetails.PaymentMethodName = pushRequest.PaymentMethodDescription;


            if (pushRequest.Amount != null)
            {
                paymentDetails.Amount = pushRequest.Amount.Value;
                paymentDetails.CurrencyCode = pushRequest.Amount.CurrencyCode;
            }
            paymentDetails.LastPaymentDate = GetLastPaymentDate(pushRequest);
            paymentDetails.PaidStatus = PaymentStatus.Paid;
            
        }

        private static DateTime GetLastPaymentDate(PaymentPushRequest pushRequest)
        {
            string paymentDate = pushRequest.PaymentDate;
            string format  = "yyyy-MM-ddTHH:mm:ss.ffzzz";
            DateTime dt = DateTime.ParseExact(paymentDate, format, null).ToUniversalTime();
            return dt;
        }
    }
}
