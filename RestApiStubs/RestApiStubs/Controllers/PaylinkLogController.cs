﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestApiStubs.Interfaces;

namespace RestApiStubs.Controllers
{
    public class PaylinkLogController : ApiController
    {
        private readonly ILogger logger;

        public PaylinkLogController(ILogger logger)
        {
            this.logger = logger;
        }

        // GET /TRAXX/PaylinkLog/{id}
        [Route("PaylinkLog/{id}")]
        public HttpResponseMessage Get([Required]string id)
        {
            logger.RootPath = logger.RootPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");

            return Request.CreateResponse(HttpStatusCode.OK, logger.GetLogForPaymentId(id));
        }
    }
}