﻿namespace RestApiStubs.Models
{
    public class Amount
    {
        public decimal Value { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class PaymentPushRequest
    {
        public string PaymentId { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodDescription { get; set; }
        public string PaymentProviderReference { get; set; }
        public Amount Amount { get; set; }
        public string PaymentDate { get; set; }
    }
}