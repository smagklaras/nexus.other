﻿using System;
using RestApiStubs.Interfaces;

namespace RestApiStubs.Models
{
    public enum PaymentStatus
    {
        Pending,
        Paid
    };

    public class OrderFilePaymentDetails
    {
        public string ReferenceNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string AnvrNumber { get; set; }
        public PaymentStatus PaidStatus { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerZipCode { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerEmail { get; set; }
        public string PaymentMethodCode { get; set; }
        public string PaymentMethodName { get; set; }

        public static OrderFilePaymentDetails Create(string paymentId, ILogger logger)
        {
            OrderFilePaymentDetails payment = logger.GetPayment(paymentId);
            if (payment != null)
            {
                return payment;
            }

            OrderFilePaymentDetails orderFilePaymentDetails = new OrderFilePaymentDetails
            {
                ReferenceNumber = paymentId,
                Amount          = 1999.99m,
                CurrencyCode    = "EUR",
                AnvrNumber      = "90681", //affiliate
                CustomerName    = "Friar Tuck",
                CustomerAddress = "Sherwood Forest",
                CustomerCity    = "Nottingham",
                CustomerZipCode = "NG1 6EL",
                CustomerEmail   = "rclarke@travix.com",
                PaidStatus      = 0,
                CreatedDate     = DateTime.Now.AddDays(-1),
                ExpiryDate      = DateTime.Now.AddDays(2),
                //InvoiceNumber = string.Format("TRAXX_STUB_{0}", paymentId)
            };

            logger.AddObject(orderFilePaymentDetails);
            return orderFilePaymentDetails;
        }
    }
}