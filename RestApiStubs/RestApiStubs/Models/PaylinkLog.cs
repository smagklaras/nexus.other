﻿using System;

namespace RestApiStubs.Models
{
    public class PaylinkLog
    {
        public string Id { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }

        public string LogMessage
        {
            get { return String.Format("PayID: {0}. Status '{1}' at {2}", Id, Message, Timestamp); }
        }
    }
}