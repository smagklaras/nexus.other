﻿using System;
using RestApiStubs.Interfaces;

namespace RestApiStubs.Models
{
    public class NullLogger : ILogger
    {
        public void AddLog(PaylinkLog log)
        {
        }

        public string GetLogForPaymentId(string id)
        {
            return String.Empty;
        }

        public string RootPath
        {
            get; set;
        }

        public void AddObject<T>(T log) where T : class
        {
            throw new NotImplementedException();
        }

        public OrderFilePaymentDetails GetPayment(string id)
        {
            throw new NotImplementedException();
        }
    }
}