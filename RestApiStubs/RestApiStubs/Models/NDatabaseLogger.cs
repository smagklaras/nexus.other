﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using NDatabase;
using NDatabase.Api;
using ILogger = RestApiStubs.Interfaces.ILogger;

namespace RestApiStubs.Models
{
    public class NDatabaseLogger : ILogger
    {
        private string rootPath;

        public virtual string RootPath
        {
            get
            {
                string dbFile = ConfigurationManager.AppSettings["DB_FILE"];
                return Path.Combine(rootPath, dbFile);
            }
            set { rootPath = value; }
        }

        public virtual void AddObject<T>(T log) where T : class
        {
            using (IOdb odb = OdbFactory.Open(RootPath))
            {
                odb.Store(log);
            }
        }

        public virtual string GetLogForPaymentId(string id)
        {
            StringBuilder sb = new StringBuilder();

            using (var odb = OdbFactory.Open(RootPath))
            {
                var logs = odb
                    .AsQueryable<PaylinkLog>()
                    .Where(log => log.Id.Equals(id));

                foreach (var log in logs)
                {
                    sb.AppendLine(log.LogMessage);
                }
            }

            return sb.ToString();
        }

        public virtual OrderFilePaymentDetails GetPayment(string id)
        {
            try
            {
                using (IOdb odb = OdbFactory.Open(RootPath))
                {
                    IQueryable<OrderFilePaymentDetails> payments = odb
                        .AsQueryable<OrderFilePaymentDetails>()
                        .Where(log => log.ReferenceNumber.Equals(id));

                    return payments.LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}