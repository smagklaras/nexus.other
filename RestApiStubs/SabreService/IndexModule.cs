﻿using System;
using Nancy;
using System.Text;
using Nancy.ModelBinding;
using RestSharp;
using SabreService.Models;
using SabreService.Models.SeatMap.Request;
using SabreService.Models.ThemeSearch;

namespace SabreService
{
    public class IndexModule : NancyModule
    {
        private string clientId = "V1:hmeftjgpy5ya5icj:DEVCENTER:EXT";
        private string secret = "7Gn4eYJo";

        public IndexModule()
        {
            Get["/"] = parameters => View["index"];

            Get["/Themes"] = _ =>
            {
                var data = GetRestResponse<HolidayThemes>("/v1/lists/supported/shop/themes");
                return Response.AsJson(data);
            };

            Get["/SearchFlights"] = o =>
            {
                SearchRequest request = this.Bind();

                StringBuilder url = new StringBuilder();

                url.Append("?origin=" + request.from);
                url.Append("&departuredate=" + request.FromDate);
                url.Append("&returndate=" + request.ToDate);
                url.Append("&lengthofstay=" + request.LengthOfStay);
                url.Append("&theme=" + request.theme.ToUpper());
                url.Append("&pointofsalecountry=NL");

                var data = GetRestResponse<Destination>("/v2/shop/flights/fares" + url.ToString());
                return Response.AsJson(data);
            };

            Get["/SeatMap"] = o =>
            {
                var seatMapUrl = "v3.0.0/book/flights/seatmaps?mode=seatmaps";
                var data = GetRestResponse<SeatMapResponse>(seatMapUrl, Method.POST);
                return Response.AsJson(data);
            };
        }
        
        private Authentication GetAuthenticationToken()
        {
            var client = new RestClient("https://api.test.sabre.com/v2/auth/token");
            
            var request = new RestRequest() { Method = Method.POST };
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Authorization", "Basic " + GetClientString());
            request.AddParameter("grant_type", "client_credentials");

            var response = client.Execute<Authentication>(request);

            return response.Data;
        }

        private T GetRestResponse<T>(string path, Method method = Method.GET) where T : new() 
        {
            var auth = GetAuthenticationToken();

            var client = new RestClient("https://api.test.sabre.com" + path);
            
            var request = new RestRequest(method);
            request.AddHeader("Authorization", "Bearer " + auth.access_token);

            var response = client.Execute<T>(request);

            return response.Data;

        }

        private string GetClientString()
        {
            var cid64 = Base64Encode(clientId);
            var sec64 = Base64Encode(secret);

            return Base64Encode(string.Format("{0}:{1}", cid64, sec64));

        }
        
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}