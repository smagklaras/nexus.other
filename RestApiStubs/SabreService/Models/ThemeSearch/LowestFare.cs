﻿using System.Collections.Generic;

namespace SabreService.Models.ThemeSearch
{
    public class LowestFare
    {
        public List<string> AirlineCodes { get; set; }
        public double Fare { get; set; }
    }
}