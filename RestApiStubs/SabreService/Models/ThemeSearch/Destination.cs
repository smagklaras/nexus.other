using System.Collections.Generic;

namespace SabreService.Models.ThemeSearch
{
    public class Destination
    {
        public string OriginLocation { get; set; }
        public List<FareInfo> FareInfo { get; set; }
        public string UsageNote { get; set; }
        public List<Link> Links { get; set; }
    }
}