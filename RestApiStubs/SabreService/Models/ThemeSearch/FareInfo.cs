using System.Collections.Generic;

namespace SabreService.Models.ThemeSearch
{
    public class FareInfo
    {
        public string CurrencyCode { get; set; }
        public object LowestNonStopFare { get; set; }
        public LowestFare LowestFare { get; set; }
        public string DestinationLocation { get; set; }
        public string DepartureDateTime { get; set; }
        public string ReturnDateTime { get; set; }
        public string Theme { get; set; }
        public List<Link> Links { get; set; }
    }
}