using System.Collections.Generic;

namespace SabreService.Models.ThemeSearch
{
    public class HolidayThemes
    {
        public List<HolidayThemes> Themes { get; set; }
        public List<Link> Links { get; set; }
    }
}