using System;

namespace SabreService
{
    public class SearchRequest
    {
        public string from { get; set; }

        public DateTime datepickerdepart { get; set; }

        public DateTime datepickerarrival { get; set; }

        public string FromDate
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd}", datepickerarrival);
            }
        }

        public string ToDate
        {
            get
            {
                return String.Format("{0:yyyy-MM-dd}", datepickerdepart); 
            }
        }

        public int LengthOfStay
        {
            get
            {
                return (datepickerdepart - datepickerarrival).Days;
            }
        }

        public string theme { get; set; }
    }
}