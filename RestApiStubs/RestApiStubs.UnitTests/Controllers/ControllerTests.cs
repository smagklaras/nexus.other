﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestApiStubs.Controllers;
using RestApiStubs.Models;

namespace RestApiStubs.UnitTests.Controllers
{
    [TestClass]
    public class ControllerTests
    {
        private Mock<NDatabaseLogger> mockLogger;
        
        [TestInitialize]
        public void InitTests()
        {
            mockLogger = new Mock<NDatabaseLogger>();
        }

        [TestCleanup]
        public void CleanupTests()
        {
            mockLogger = null;
        }

        private PaylinkController PaylinkController
        {
            get
            {
                PaylinkController controller = new PaylinkController(mockLogger.Object)
                {
                    Request = new HttpRequestMessage()
                };
                controller.Request.SetConfiguration(new HttpConfiguration());

                return controller;
            }
        }

        [TestMethod]
        public void TestGetPaylinkObject_Success()
        {
            string payRef = "mockPayRef";
            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Get(payRef);
            Assert.IsTrue(reply.StatusCode == HttpStatusCode.OK);
        }

        [TestMethod]
        public void TestSetPaylinkToPaid_Success()
        {
            string payRef = "mockPayRef";
            string action = "Paid";
            OrderFilePaymentDetails mockPaymentDetails = GetMockPaymentDetails(payRef);

            mockLogger
                .Setup(it => it.GetPayment(payRef))
                .Returns(mockPaymentDetails)
                .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<PaylinkLog>()))
               .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(mockPaymentDetails))
               .Verifiable();

            PaymentPushRequest paymentPushRequest = GetMockPaymentPushRequest(payRef);
            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Post(payRef, action, paymentPushRequest);

            Assert.IsTrue(reply.StatusCode == HttpStatusCode.OK);

            mockLogger.Verify(it => it.GetPayment(payRef), Times.Once);
            mockLogger.Verify(it => it.AddObject(It.IsAny<PaylinkLog>()), Times.Once);
            mockLogger.Verify(it => it.AddObject(mockPaymentDetails), Times.Once);

        }

        [TestMethod]
        public void TestSetPaylinkPaid_InvalidAction()
        {
            string payRef = "mockPayRef";
            string action = "invalidAction";
            OrderFilePaymentDetails mockPaymentDetails = GetMockPaymentDetails(payRef);

            mockLogger
                .Setup(it => it.GetPayment(payRef))
                .Returns(mockPaymentDetails)
                .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<PaylinkLog>()))
               .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(mockPaymentDetails))
               .Verifiable();

            PaymentPushRequest paymentPushRequest = GetMockPaymentPushRequest(payRef);
            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Post(payRef, action, paymentPushRequest);

            Assert.IsTrue(reply.StatusCode == HttpStatusCode.BadRequest);
            mockLogger.Verify(it => it.AddObject(It.IsAny<PaylinkLog>()), Times.Once);
            mockLogger.Verify(it => it.GetPayment(payRef), Times.Never);
            mockLogger.Verify(it => it.AddObject(mockPaymentDetails), Times.Never);
        }

        [TestMethod]
        public void TestSetPaylinkPaid_InvalidRequest()
        {
            string payRef = "mockPayRef";
            string action = "Paid";
            
            mockLogger
                .Setup(it => it.GetPayment(It.IsAny<string>()))
                .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<PaylinkLog>()))
               .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<OrderFilePaymentDetails>()))
               .Verifiable();

            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Post(payRef, action, null);

            Assert.IsTrue(reply.StatusCode == HttpStatusCode.BadRequest);
            mockLogger.Verify(it => it.AddObject(It.IsAny<PaylinkLog>()), Times.Once);
            mockLogger.Verify(it => it.GetPayment(payRef), Times.Never);
            mockLogger.Verify(it => it.AddObject(It.IsAny<OrderFilePaymentDetails>()), Times.Never);
        }

        [TestMethod]
        public void TestSetPaylinkPaid_NotFound()
        {
            string payRef = "mockPayRef";
            string action = "Paid";
            OrderFilePaymentDetails mockPaymentDetails = GetMockPaymentDetails(payRef);

            mockLogger
                .Setup(it => it.GetPayment(payRef))
                .Returns((OrderFilePaymentDetails) null)
                .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<PaylinkLog>()))
               .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(mockPaymentDetails))
               .Verifiable();

            PaymentPushRequest paymentPushRequest = GetMockPaymentPushRequest(payRef);
            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Post(payRef, action, paymentPushRequest);

            Assert.IsTrue(reply.StatusCode == HttpStatusCode.NotFound);
            mockLogger.Verify(it => it.AddObject(It.IsAny<PaylinkLog>()), Times.Once);
            mockLogger.Verify(it => it.GetPayment(payRef), Times.Once);
            mockLogger.Verify(it => it.AddObject(mockPaymentDetails), Times.Never);
        }

        [TestMethod]
        public void TestSetPaylinkPaid_Gone()
        {
            string payRef = "mockPayRef";
            string action = "Paid";
            OrderFilePaymentDetails mockPaymentDetails = GetMockPaymentDetails(payRef);
            mockPaymentDetails.PaidStatus = PaymentStatus.Paid;

            mockLogger
                .Setup(it => it.GetPayment(payRef))
                .Returns(mockPaymentDetails)
                .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(It.IsAny<PaylinkLog>()))
               .Verifiable();

            mockLogger
               .Setup(it => it.AddObject(mockPaymentDetails))
               .Verifiable();

            PaymentPushRequest paymentPushRequest = GetMockPaymentPushRequest(payRef);
            PaylinkController controller = PaylinkController;
            HttpResponseMessage reply = controller.Post(payRef, action, paymentPushRequest);

            Assert.IsTrue(reply.StatusCode == HttpStatusCode.Gone);
            mockLogger.Verify(it => it.AddObject(It.IsAny<PaylinkLog>()), Times.Once);
            mockLogger.Verify(it => it.GetPayment(payRef), Times.Once);
            mockLogger.Verify(it => it.AddObject(mockPaymentDetails), Times.Never);
        }

        private static OrderFilePaymentDetails GetMockPaymentDetails(string paymentId)
        {
            return new OrderFilePaymentDetails
            {
                ReferenceNumber = paymentId,
                Amount          = 1999.99m,
                CurrencyCode    = "EUR",
                AnvrNumber      = "90681", //affiliate
                CustomerName    = "Friar Tuck",
                CustomerAddress = "Sherwood Forest",
                CustomerCity    = "Nottingham",
                CustomerZipCode = "NG1 6EL",
                CustomerEmail   = "rclarke@travix.com",
                PaidStatus      = 0,
                CreatedDate     = DateTime.Now.AddDays(-1),
                ExpiryDate      = DateTime.Now.AddDays(2),
            };
        }
        private static PaymentPushRequest GetMockPaymentPushRequest(string payRef)
        {
            PaymentPushRequest mockPaymentPushRequest = new PaymentPushRequest
            {
                PaymentId = payRef,
                Amount = new Amount { CurrencyCode = "EUR", Value = 100.0m },
                PaymentDate = DateTime.Now.AddMinutes(-1)
                    .ToUniversalTime()
                    .ToString("yyyy-MM-ddTHH:mm:ss.ffzzz"),
                PaymentMethodCode = "VISA",
                PaymentMethodDescription = "VISA CreditCard",
                PaymentProviderReference = string.Format("provider_{0}", payRef)
            };
            return mockPaymentPushRequest;
        }
    }
}
