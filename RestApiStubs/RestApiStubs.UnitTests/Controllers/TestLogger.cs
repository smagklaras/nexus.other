using System;
using System.Collections.Generic;
using RestApiStubs.Interfaces;
using RestApiStubs.Models;

namespace RestApiStubs.UnitTests.Controllers
{
    internal class TestLogger : ILogger
    {
        readonly Dictionary<string, string> logs = new Dictionary<string, string>();

        public void AddLog(PaylinkLog log)
        {
            logs.Add(log.Id, log.LogMessage);
        }

        public string GetLogForPaymentId(string id)
        {
            if (logs.ContainsKey(id))
            {
                return logs[id];
            }

            throw new KeyNotFoundException();
        }

        public string RootPath
        {
            get; set;
        }

        public void AddObject<T>(T log) where T : class
        {
            throw new NotImplementedException();
        }

        public OrderFilePaymentDetails GetPayment(string id)
        {
            return new OrderFilePaymentDetails
            {
                ReferenceNumber = id,
                Amount = 1999.99m,
                CurrencyCode = "EUR",
                AnvrNumber = "90681", //affiliate
                CustomerName = "Friar Tuck",
                CustomerAddress = "Sherwood Forest",
                CustomerCity = "Nottingham",
                CustomerZipCode = "NG1 6EL",
                CustomerEmail = "rclarke@travix.com",
                PaidStatus = 0,
                CreatedDate = DateTime.Now.AddDays(-1),
                ExpiryDate = DateTime.Now.AddDays(2),
                //InvoiceNumber = string.Format("TRAXX_STUB_{0}", paymentId)
            };
        }
    }
}