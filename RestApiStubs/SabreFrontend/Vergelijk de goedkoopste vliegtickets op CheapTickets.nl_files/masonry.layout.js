/**
 * Helper script for masonry style layouts.
 * It basically removes whitespace between elements in the same column
 * so you can mix a horizontal grid with a vertical grid
 *
 * @class Masonry
 * @constructor
 * @public
 * @example
 *
 * You only need to include the script in the <head>,
 * it runs by itself on slotRendered/resize
 *
 */
function setMasonry() {

  /**
   * Returns the given number floored down by 10ths,
   * so you can compare simular numbers. This because percentage based
   * sizes converted to pixel values can be unreliable and off by a pixel
   *
   * @method floor
   * @param {Number}
   * @return {Number}
   *
   * @example
   *
   *    input: 42
   *    output: 40
   *
   *    input: 49.9999
   *    output: 40
   */
  function floor(num){ return Math.floor(num / 10) * 10 }

  /**
   * Get current screen size
   *
   * @method getBreakpoint
   * @return {String} Return current breapoint defined by media-queries
   */
  function getBreakpoint() {
    if (typeof window !== 'undefined') {
      return window.getComputedStyle(
        document.querySelector('body'), ':before'
      ).getPropertyValue('content').replace(/["']/g, '');
    }
  }

  /**
   * Default settings
   * @TODO: create default object with param overwrite
   */
  var bpColCount = {tiny: 1, small: 1, medium: 2, large: 3, xlarge: 3};

  var container = document.querySelector('#masonry');
  var containerWidth = container.clientWidth;
  var items = container.querySelectorAll('.masonry-item');
  var colCount = bpColCount[getBreakpoint() || 'large'];
  var colWidth = Math.round(containerWidth / colCount);
  var index = 0;
  var colTop = [];


  /* Iterate through items and set marginTop to remove whitespace between items */
  for(var i = 0; i < items.length; i++){
    var cItem = items[i];

    // Only apply if item is actually visible
    if(cItem.clientWidth > 0){

      // Reset marginTop
      cItem.style.marginTop = 0;

      // Check the current column
      var cCol = Math.round(colCount / (containerWidth / (cItem.offsetLeft - container.offsetLeft)));

      // Check the last column if it spans multiple columns (66% item for example)
      var endCol = cCol + colCount - Math.round(containerWidth / cItem.clientWidth);

      // Use the closest previous item for gap calculation
      var tarCol = colTop[endCol] > colTop[cCol] ? endCol : cCol;

      // Calculate the gap between current and previous item in the same column
      var tarMargin = colTop[tarCol] ? colTop[tarCol] - (cItem.offsetTop - container.offsetTop) : 0;

      // Only manipulate marginTop if it's not a full-width item
      if(Math.round(containerWidth / cItem.clientWidth) > 1){
        cItem.style.marginTop = tarMargin + 'px';
      }

      // Update all columns depending on the span of the current item (66% needs to update 2 columns)
      for(var span = 0; span < Math.round(cItem.clientWidth / colWidth); span++){
        colTop[cCol + span] = cItem.offsetTop - container.offsetTop + cItem.clientHeight;
      }
    }
  }
}

/* Update every 100ms instead of inmediately to increase window resize performance */
var masonryTimer;
function resetMasonryTimer(){
  if(masonryTimer) clearTimeout(masonryTimer);
  masonryTimer = setTimeout(setMasonry, 100);
}

/* Update when a new slotComponent is rendered/mounted */
document.addEventListener('slotRendered', resetMasonryTimer);

/* Update when all content, images and assets are loaded */
document.addEventListener('DOMContentLoaded', resetMasonryTimer);

/* Update when resizing window */
window.addEventListener('resize', resetMasonryTimer);

/* Run masonry on page load */
setMasonry();
